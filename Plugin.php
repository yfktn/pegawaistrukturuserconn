<?php namespace Yfktn\Pegawaistrukturuserconn;

use System\Classes\PluginBase;
use System\Classes\PluginManager;
use Yfktn\Pegawaistrukturuserconn\Classes\TraitConnectorRainLabBlog;

class Plugin extends PluginBase
{
    use \Yfktn\Pegawaistrukturuserconn\Classes\TraitConnectorPegawai;
    use \Yfktn\Pegawaistrukturuserconn\Classes\TraitConnectorStrukturOrgTulisan;
    use TraitConnectorRainLabBlog;

    private $isPluginExist = [
        'RainLab.Blog' => false,
        'Yfktn.Tulisan' => false,
    ];

    private function checkIfPluginExist() 
    {
        $pluginManager = PluginManager::instance();
        // check bila plugin tulisan ada
        if ($pluginManager->hasPlugin('Yfktn.Tulisan')) {
            $this->isPluginExist['Yfktn.Tulisan'] = true;
        }
        // check bila rainblog ada
        if ($pluginManager->hasPlugin('RainLab.Blog')) {
            $this->isPluginExist['RainLab.Blog'] = true;
        }
    }
    
    public function registerComponents()
    {
        $comps = [];
        if($this->isPluginExist['RainLab.Blog']) {
            $comps['Yfktn\PegawaiStrukturuserconn\Components\RainPostsOnUnitKerja'] = 'rainPostsOnUnitKerja';
        }
        if ($this->isPluginExist['Yfktn.Tulisan']) {
            $comps['Yfktn\PegawaiStrukturuserconn\Components\DaftarTulisanDiUnitKerja'] = 'daftarTulisanDiUnitKerja';
        }
        return $comps;
    }

    public function registerSettings()
    {
    }
    
    public function boot() {
        $this->checkIfPluginExist();
        $this->connectorPegawai();

        // check bila plugin tulisan ada
        if($this->isPluginExist['Yfktn.Tulisan']) {
            $this->connectorStrukturOrgTulisan();
        }
        // check bila rainblog ada
        if($this->isPluginExist['RainLab.Blog']) {
            $this->connectorRainLabBlog();
        }
    }
}
