Ini membutuhkan plugin OctoberCMS untuk Yfktn.Pegawai, Yfktn.StrukturOrg.

Selanjutnya tulisan merujuk ke Yfktn.Tulisan dan blog merujuk ke RainLab.Blog.

Plugin ini Memberikan fasilitas untuk:
- menampilkan tulisan / blog berdasarkan unit kerja user menggunakan component yang tersedia
- melakukan filter pada bagian tampilan tulisan / blog, pada backend
- apabila tidak membutuhkan entri pegawai, maka pada UI bisa lakukan entri untuk User login dan unit kerja saja, namun sayangnya hingga saat ini Yfktn.Pegawai adalah requirement dan tidak bisa dihilangkan / harus diinstal
- agar bisa menggunakan ini maka entri pada Yfktn.StrukturOrg harus terlebih dahulu dimasukkan