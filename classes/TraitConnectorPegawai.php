<?php namespace Yfktn\Pegawaistrukturuserconn\Classes;

use Yfktn\Pegawai\Models\Pegawai as PegawaiModel;
use Yfktn\Pegawai\Controllers\Pegawai as PegawaiController;
use Backend\Models\User as BackendUserModel;
use Event;
/**
 * Tambahkan ini ke plugin class serta panggil dari boot untuk menambahkan fungsi
 * tambahan di pegawai
 *
 * @author toni
 */
trait TraitConnectorPegawai {
    /**
     * Buat list pegawai supaya sesuai dengan yang login, sehingga yang tampil
     * hanya milik si user yang login, atau bisa mengakses ke user yang lain
     * sesuai dengan hak akses!
     */
    protected function connectorPegawai() {
        // jangan menampilkan user selain si pegawai yang login ini
        // buat dahulu relasi ke pegawai!
        BackendUserModel::extend(function($model) {
            $model->belongsToMany['pegawai'] = [
                'Yfktn\Pegawai\Models\Pegawai',
                'table' => 'yfktn_pegawaistrukturuserconn_utama',
                'key' => 'user_id',
                'otherKey' => 'pegawai_id'
            ];
            $model->belongsToMany['unit_kerja'] = [
                'Yfktn\StrukturOrg\Models\StrukturOrg',
                'table' => 'yfktn_pegawaistrukturuserconn_utama',
                'key' => 'user_id',
                'otherKey' => 'strukturorg_id'
            ];
        });
        
        // tambahkan link pegawai ke struktur organisasi supaya bisa mendapatkan
        // di mana user ini bekerja saat ini
        PegawaiModel::extend(function($model) {
            $model->belongsToMany['unit_kerja'] = [
                'Yfktn\StrukturOrg\Models\StrukturOrg',
                'table' => 'yfktn_pegawaistrukturuserconn_utama',
                'key' => 'pegawai_id',
                'otherKey' => 'strukturorg_id'
            ];
        });
        
        // hanya tampilkan milik pegawai sendiri saja jangan yang lain
        Event::listen('backend.list.extendQuery', function($widget, $query) {
            if(
                $widget->model instanceof PegawaiModel && 
                !\BackendAuth::getUser()->hasAccess('yfktn.pegawai.atur_pegawai_lainnya') 
            ) {
                if(($be = \BackendAuth::getUser()->pegawai()->first())!= null) {
                    $query->where('id', $be->id);
                } else {
                    // user ini belum diasosiasikan dengan pegawai
                    $query->where('id', 0);
                }
            }
        });
        
        // tampilkan pada unit mana dia saat ini
        Event::listen('backend.list.extendColumns', function($widget) {
            if(!$widget->getController() instanceof PegawaiController) {
                return;
            }
            
            if(!$widget->model instanceof PegawaiModel) {
                return;
            }
            // tambahkan kolom unit kerja di tampilan daftar pegawai
            $widget->addColumns([
                'unit_kerja' => [
                    'label' => 'Unit Kerja',
                    'relation' => 'unit_kerja',
                    'select' => 'nama'
                ]
            ]);
        });
        
        Event::listen('yfktn.pegawai.formExtendQuery', function($query) {
            // lakukan proses untuk melakukan validasi user yang bisa mengakses
            // jangan berikan akses ke user yang lain, apabila user ini tidak
            // memiliki hak akses untuk mengakses user yang lain!
            if(!\BackendAuth::getUser()
                    ->hasAccess('yfktn.pegawai.atur_pegawai_lainnya') ) {
                if(($be = \BackendAuth::getUser()->pegawai()->first())!= null) {
                    $query->where('id', $be->id);
                } else {
                    // user ini belum diasosiasikan dengan pegawai
                    $query->where('id', 0);
                }
            }
        });
        
    }
}
