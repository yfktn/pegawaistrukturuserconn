<?php namespace Yfktn\Pegawaistrukturuserconn\Classes;
/**
 * Jangan tampilkan punya unit lain untuk Rainlab blog
 */
use Rainlab\Blog\Models\Post as RainlabBlogPostModel;
use Rainlab\Blog\Controllers\Posts as RainlabBlogPostController;
use Backend\Models\User as BackendUserModel;
use BackendAuth;
use Event;

trait TraitConnectorRainLabBlog {
    protected function connectorRainLabBlog() {
        RainlabBlogPostModel::extend(function($model) {
            // bug: di bawah ini tidak benar karena semestinya yang konek ke pivot adalah id rainlab.blog
            // semestinya ini lewat backenduser.unit_kerja!
            // $model->belongsToMany['unit_kerja'] = [
            //     'Yfktn\StrukturOrg\Models\StrukturOrg',
            //     'table' => 'yfktn_pegawaistrukturuserconn_utama',
            //     'key' => 'user_id',
            //     'otherKey' => 'strukturorg_id'
            // ];
        });

        // hanya tampilkan milik pegawai sendiri saja jangan yang lain
        Event::listen('backend.list.extendQuery', function($widget, $query) {
            if( $widget->model instanceof RainlabBlogPostModel ) {
                // cari tulisan dari user dengan unit kerja yang sama dengan user
                // BILA punya hak akses untuk mengakses user lain, maka:
                if(BackendAuth::getUser()->hasAnyAccess(['rainlab.blog.access_other_posts']) 
                        && !BackendAuth::getUser()->isSuperUser()) { // dan user ini bukan super admin
                    // check kembali bila user ini punya hak akses untuk mengakses
                    // punya user unit lain
                    if( BackendAuth::getUser()->hasAnyAccess([
                        'yfktn.pegawaistrukturuserconn.akses_tulisan_unit_lain']) ) {
                        // nothing to do!
                    }
                    // dapatkan induknya, sementara ini dengan induknya dulu
                    // sebagai referensi penentuannya
                    elseif(($unitkerja = BackendAuth::getUser()->unit_kerja()->first())!=null) {
                        $parent = $unitkerja->parent_id == null? $unitkerja->id: $unitkerja->parent_id;
                        $usersid = []; // dapatkan user id yang masuk dalam unit kerja yang sama
                        $usersid = BackendUserModel::whereHas('unit_kerja', function($query) use ($parent){
                            $query->where('parent_id', $parent)
                                // bug duplicate `id` error setelah yfktn_pegawaistrukturuserconn_utama memiliki `id`
                                // harus explicit dideklarasikan nama table nya di sini!
                                ->orWhere('yfktn_strukturorg_utama.id', $parent);
                        })->lists('id');
                        // foreach($userscoll as $u) {
                        //     $usersid[] = $u->id;
                        // }
                        // tambahkan pada query
                        $query->whereIn('user_id', $usersid);
                    } else {
                        // bila belum diasosiasikan dengan unit kerja, tidak
                        // usah ditampilkan apa-apa, tampilkan tulisan nya saja
                        $query->where('user_id', BackendAuth::getUser()->id);
                    }
                }
            }
        });
    }
}
