<?php namespace Yfktn\Pegawaistrukturuserconn\Classes;
use Yfktn\Tulisan\Models\Tulisan as TulisanModel;
use Yfktn\Tulisan\Controllers\Tulisan as TulisanController;
use Backend\Models\User as BackendUserModel;
use Event;
use BackendAuth;
/**
 * Lakukan link di antara tulisan dan struktur organisasi, salah satunya adalah
 * jangan user dapat mengakses milik user dari struktur organsasi lain
 *
 * @author The Happy Dude
 */
trait TraitConnectorStrukturOrgTulisan {
    
    /**
     * Penulis dari salah satu bagian tidak dapat mengakses milik bagian yang
     * lainnya.
     */
    protected function connectorStrukturOrgTulisan() {
        // buat link ke struktur supaya bisa mendapatkan di mana unit kerja
        // ini berada
        BackendUserModel::extend(function($model) {
            $model->belongsToMany['unit_kerja'] = [
                'Yfktn\StrukturOrg\Models\StrukturOrg',
                'table' => 'yfktn_pegawaistrukturuserconn_utama',
                'key' => 'user_id',
                'otherKey' => 'strukturorg_id'
            ];
        });
        // batasi tampilan supaya tidak muncul punya unit kerja yang lain
        
        // hanya tampilkan milik pegawai sendiri saja jangan yang lain
        Event::listen('backend.list.extendQuery', function($widget, $query) {
            if( $widget->model instanceof TulisanModel ) {
                // cari tulisan dari user dengan unit kerja yang sama dengan user
                // BILA punya hak akses untuk mengakses user lain, maka:
                if(BackendAuth::getUser()->hasAnyAccess(['yfktn.tulisan.tulisan_akses_user_lain']) 
                        && !BackendAuth::getUser()->isSuperUser()) { // dan user ini bukan super admin
                    // check kembali bila user ini punya hak akses untuk mengakses
                    // punya user unit lain
                    if( BackendAuth::getUser()->hasAnyAccess([
                        'yfktn.pegawaistrukturuserconn.akses_tulisan_unit_lain']) ) {
                        // nothing to do!
                    }
                    // dapatkan induknya, sementara ini dengan induknya dulu
                    // sebagai referensi penentuannya
                    elseif(($unitkerja = BackendAuth::getUser()->unit_kerja()->first())!=null) {
                        $parent = $unitkerja->parent_id == null? $unitkerja->id: $unitkerja->parent_id;
                        $usersid = []; // dapatkan user id yang masuk dalam unit kerja yang sama
                        $userscoll = BackendUserModel::whereHas('unit_kerja', function($query) use ($parent){
                            $query->where('parent_id', $parent)
                                // bug duplicate `id` error setelah yfktn_pegawaistrukturuserconn_utama memiliki `id`
                                // harus explicit dideklarasikan nama table nya di sini!
                                ->orWhere('yfktn_strukturorg_utama.id', $parent);
                        })->get();
                        foreach($userscoll as $u) {
                            $usersid[] = $u->id;
                        }
                        // tambahkan pada query
                        $query->whereIn('user_id', $usersid);
                    } else {
                        // bila belum diasosiasikan dengan unit kerja, tidak
                        // usah ditampilkan apa-apa, tampilkan tulisan nya saja
                        $query->where('user_id', BackendAuth::getUser()->id);
                    }
                }
            }
        });
    }
}
