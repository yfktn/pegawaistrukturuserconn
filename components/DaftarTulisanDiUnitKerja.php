<?php namespace Yfktn\Pegawaistrukturuserconn\Components;

use Yfktn\Tulisan\Components\TulisanList as TulisanListComponent;
use Backend\Models\User as BackendUserModel;
use Yfktn\Tulisan\Models\Tulisan as TulisanModel;

class DaftarTulisanDiUnitKerja extends TulisanListComponent {

    public function componentDetails()
    {
        return [
            'name'        => 'Tulisan Di Unit Kerja',
            'description' => 'Tampilkan daftar tulisan berdasar unit kerja'
        ];
    }


    public function defineProperties()
    {
        $parentprop = parent::defineProperties();
        return [
            'unitKerjaFilter' => [
                'title' => 'Filter Unit Kerja',
                'description' => 'Filter unit kerja slug',
                'type' => 'string',
                'default' => '{{ :slugunitkerja }}'
            ]
        ] + $parentprop;
    }

    /**
     * Override siapkan variablenya
     * @return void 
     */
    protected function siapkanVariable()
    {
        $this->page['unitKerjaParam'] = $this->paramName('unitKerjaFilter');
        $this->page['unitKerjaValue'] = $this->property('unitKerjaFilter');
        parent::siapkanVariable();
    }


    /**
     * Dapatkan user yang ada dalam suatu unit kerja
     *
     * @param [type] $unitKerjaSlug
     * @return void
     */
    protected function getUserInUnitKerja($unitKerjaSlug)
    {
        // TODO: Masukkan dalam cache
        $backendUserModel = BackendUserModel::whereHas('unit_kerja', function ($query) use ($unitKerjaSlug) {
            $query->where('slug', $unitKerjaSlug);
        });
        return $backendUserModel->lists('id');
    }

    /**
     * Lakukan override loading tulisan
     * @return mixed 
     */
    public function loadTulisan()
    {
        $unitKerjaSlug = $this->page['unitKerjaValue'];
        $posts = TulisanModel::with(['gambar_header', 'kategori'])
            ->yangSudahDitampilkan()
            ->whereHas('user.unit_kerja', function($query) use($unitKerjaSlug) {
                $query->where('slug', $unitKerjaSlug);
            })
            ->listDiFrontEnd([
                'page' => $this->page['halamanAktif'],
                'jumlahItemPerHalaman' => $this->page['jumlahItemPerHalaman'],
                'filter' => [
                    'kategori_slug_filter' => $this->page['filterKategori'],
                    'kategori_slug_except' => $this->page['abaikanKategori']
                ],
                'order' => [
                    'tgl_tampil' => 'DESC'
                ]
            ]);
        // kita dapatkan daftar maka proses untuk melakukan setting sesuai 
        // dengan url daripada page detailnya!
        // TODO: gunakan getProperty untuk mengdapatkan setting url id dan slug
        $posts->each(function ($post) {
            $post->setUrl($this->page['halamanDetail'], $this->controller, [
                'id' => 'id', 'slug' => 'slug'
            ]);
        });
        return $posts;
    }
}