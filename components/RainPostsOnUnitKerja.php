<?php namespace Yfktn\Pegawaistrukturuserconn\Components;

use RainLab\Blog\Models\Post as BlogPost;
use RainLab\Blog\Components\Posts as RainPostsComponent;
use Backend\Models\User as BackendUserModel;

/**
 * Class ini membuat komponen untuk menampilkan daftar blog berdasarkan unit kerja terpilih! Di mana
 * komponen yang digunakan adalah milik plugin Rainlab.Blog
 */
class RainPostsOnUnitKerja extends RainPostsComponent {

    public function componentDetails()
    {
        return [
            'name'        => 'RainLab Posts Unit Kerja',
            'description' => 'Tampilkan daftar rainlab.blog berdasar unit kerja'
        ];
    }

    public function defineProperties()
    {
        $parentprop = parent::defineProperties();
        return [
            'unitKerjaFilter' => [
                'title' => 'Filter Unit Kerja',
                'description' => 'Filter unit kerja slug',
                'type' => 'string',
                'default' => '{{ :slugunitkerja }}'
            ]
        ] + $parentprop;
    }

    protected function prepareVars() {
        $this->page['unitKerjaParam'] = $this->paramName('unitKerjaFilter');
        $this->page['unitKerjaValue'] = $this->property('unitKerjaFilter');
        parent::prepareVars();
    }

    /**
     * Dapatkan user yang ada dalam suatu unit kerja
     *
     * @param [type] $unitKerjaSlug
     * @return void
     */
    protected function getUserInUnitKerja($unitKerjaSlug) {
        // TODO: Masukkan dalam cache
        $backendUserModel = BackendUserModel::whereHas('unit_kerja', function($query) use($unitKerjaSlug) {
            $query->where('slug', $unitKerjaSlug);
        });
        return $backendUserModel->lists('id');
    }

    protected function listPosts()
    {
        $category = $this->category ? $this->category->id : null;

        /*
         * List all the posts, eager load their categories
         */
        $isPublished = !$this->checkEditor();

        $unitKerjaSlug = $this->page['unitKerjaValue'];
        $userInUnitKerja = $this->getUserInUnitKerja($unitKerjaSlug);

        $posts = BlogPost::with('categories')
        // ->whereIn('user_id', $userInUnitKerja)
        ->whereHas('user.unit_kerja', function($query) use($unitKerjaSlug) {
            $query->where('slug', $unitKerjaSlug);
        })
        ->listFrontEnd([
            'page'             => $this->property('pageNumber'),
            'sort'             => $this->property('sortOrder'),
            'perPage'          => $this->property('postsPerPage'),
            'search'           => trim(input('search')),
            'category'         => $category,
            'published'        => $isPublished,
            'exceptPost'       => is_array($this->property('exceptPost'))
                ? $this->property('exceptPost')
                : preg_split('/,\s*/', $this->property('exceptPost'), -1, PREG_SPLIT_NO_EMPTY),
            'exceptCategories' => is_array($this->property('exceptCategories'))
                ? $this->property('exceptCategories')
                : preg_split('/,\s*/', $this->property('exceptCategories'), -1, PREG_SPLIT_NO_EMPTY),
        ]);

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
        $posts->each(function($post) {
            $post->setUrl($this->postPage, $this->controller);

            $post->categories->each(function($category) {
                $category->setUrl($this->categoryPage, $this->controller);
            });
        });

        return $posts;
    }


}