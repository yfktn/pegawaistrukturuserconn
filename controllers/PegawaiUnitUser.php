<?php namespace Yfktn\Pegawaistrukturuserconn\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class PegawaiUnitUser extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'yfktn.pegawaistrukturuserconn.admin_pegawai' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Yfktn.Pegawaistrukturuserconn', 'main-menu-pegstrukuse');
    }
}
