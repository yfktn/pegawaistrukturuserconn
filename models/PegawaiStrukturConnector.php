<?php namespace Yfktn\Pegawaistrukturuserconn\Models;

use Db;
use Model;
use PhpParser\Node\Expr\Cast\Object_;

/**
 * Model
 */
class PegawaiStrukturConnector extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    const JABATAN_KEPALA = 'KEPALA';
    const JABATAN_STAF = 'STAFF';
    const JABATAN_JFT = 'JFT';

    // protected $primaryKey = 'pegawai_id';
    // public $incrementing = false;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_pegawaistrukturuserconn_utama';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'pegawai_id' => "nullable|unique:yfktn_pegawaistrukturuserconn_utama",
        'user_id' => "nullable|unique:yfktn_pegawaistrukturuserconn_utama"
    ];
    
    public $belongsTo = [
        'user' => [ 'Backend\Models\User', 'key' => 'user_id' ],
        'pegawai' => [ 'Yfktn\Pegawai\Models\Pegawai', 'key' => 'pegawai_id' ],
        'unit' => [ 'Yfktn\StrukturOrg\Models\StrukturOrg', 'key' => 'strukturorg_id' ]
    ];
    
    public function getJabatanStrukturOrg() {
        return [
            'KEPALA' => 'Kepala',
            'STAFF' => 'Staff',
            'JFT' => 'Jasa Fungsional Tertentu'
        ];
    }

    /**
     * Digunakan untuk menampilkan user yang belum direlasikan dengan pegawai / OPD
     * @param mixed $fieldName 
     * @param mixed $value 
     * @param mixed $formData 
     * @return mixed untuk ditampilkan di dropdown
     */
    public function loadUserIdNya($fieldName, $value, $formData)
    {
        // load user yang belum masuk ke sini
        return Db::table('backend_users')
            ->leftJoin('yfktn_pegawaistrukturuserconn_utama', 'yfktn_pegawaistrukturuserconn_utama.user_id', '=', 'backend_users.id')
            ->whereNull('yfktn_pegawaistrukturuserconn_utama.user_id')
            ->orWhere('backend_users.id', '=', $formData->user_id)
            ->selectRaw('backend_users.id, concat(backend_users.login, " (", backend_users.email, ")") as logininfo')
            ->pluck('logininfo', 'backend_users.id');
    }

    /**
     * Pastikan ada yang diisi, 2 bagian relasi!
     */
    public function beforeSave()
    {
        
    }
    
        
}
