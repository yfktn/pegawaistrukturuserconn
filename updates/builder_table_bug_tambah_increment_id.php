<?php namespace Yfktn\Pegawaistrukturuserconn\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableBugTambahIncrementId extends Migration
{
    public function up()
    {
        Schema::table('yfktn_pegawaistrukturuserconn_utama', function ($table) {
            $table->increments('id');
        });
    }

    public function down()
    {
        Schema::table('yfktn_pegawaistrukturuserconn_utama', function ($table) {
            $table->dropPrimary('id');
            $table->dropColumn('id');
        });
    }
}