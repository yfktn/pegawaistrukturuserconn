<?php namespace Yfktn\Pegawaistrukturuserconn\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnPegawaistrukturuserconnUtama extends Migration
{
    public function up()
    {
        Schema::create('yfktn_pegawaistrukturuserconn_utama', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('pegawai_id')->nullable()->unsigned()->index();
            $table->integer('strukturorg_id')->nullable()->unsigned()->index();
            $table->string('strukturorg_jabatan', 25)->nullable()->default('STAFF');
            $table->integer('user_id')->nullable()->unsigned()->index();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_pegawaistrukturuserconn_utama');
    }
}